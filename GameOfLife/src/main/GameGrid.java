package main;

import java.util.ArrayList;

public class GameGrid
{
	private GameCell[][] cellSpace;
	public ArrayList<ArrayList<Integer>> cellSpaceInt;
	private final int GRID_HEIGHT, GRID_WIDTH;

	public GameGrid(ArrayList<String> lines, final int width, final int height)
	{
		GRID_WIDTH = width;
		GRID_HEIGHT = height;
		cellSpaceInt = new ArrayList<ArrayList<Integer>>();
		for(int i = 0; i<height; ++i){
			cellSpaceInt.add(new ArrayList<Integer>(width));
		}

		cellSpace = new GameCell[GRID_HEIGHT][GRID_WIDTH];
		try {
			for (int i = 0; i<lines.size(); ++i){
				String str = lines.get(i);
				if (str != null){
					String[] initialCellStates = str.split(","); 
					
					for (int j = 0; j<GRID_WIDTH; ++j){
						cellSpace[i][j] = new GameCell(Integer.parseInt(initialCellStates[j]));
						cellSpaceInt.get(i).add(Integer.parseInt(initialCellStates[j]));
					}
				}
			}
		} catch (Exception ex) {
				ex.printStackTrace();
		}
		for (GameCell[] row : cellSpace){
			for (GameCell cell : row){
				System.out.print(cell.getCurrentState() + " ");
			}
			System.out.println();
		}
	}

	public void FinalizeStep(){
		for(int i = 0; i<GRID_HEIGHT; ++i){
			ArrayList<Integer> replacementList = new ArrayList<Integer>();
			for(int j = 0; j<GRID_WIDTH; ++j){
				replacementList.add(cellSpace[i][j].swapStates());
			}
			cellSpaceInt.set(i, replacementList);
		}
	}
	
	

	public void nextStep(){

		for(int i = 1; i<GRID_HEIGHT-1; ++i){
			for (int j = 1; j < GRID_WIDTH-1; ++j){
				centerCellNextStep(cellSpace[i][j], i, j);
			}
		}

		for (int i = 1; i<GRID_WIDTH-1; ++i){
			int count = bottomGetState(0,i)+leftGetState(0,i)+rightGetState(0,i)+bottomRightGetState(0,i)+bottomLeftGetState(0,i);
			cellSpace[0][i].setNextState(count);
		}
		for (int i = 1; i<GRID_WIDTH-1; ++i){
			int count = leftGetState(GRID_HEIGHT-1, i)+rightGetState(GRID_HEIGHT-1, i)+topRightGetState(GRID_HEIGHT-1, i)+topGetState(GRID_HEIGHT-1, i)+topLeftGetState(GRID_HEIGHT-1, i);
			cellSpace[GRID_HEIGHT-1][i].setNextState(count);
   
		}
		for (int i = 1; i<GRID_HEIGHT-1; ++i){
			int count = topGetState(i, 0)+topRightGetState(i, 0)+rightGetState(i, 0)+bottomRightGetState(i, 0)+bottomGetState(i, 0);
			cellSpace[i][0].setNextState(count);
		}
		for (int i = 1; i<GRID_HEIGHT-1; ++i){
			int count = topGetState(i, GRID_WIDTH-1)+topLeftGetState(i, GRID_WIDTH-1)+leftGetState(i, GRID_WIDTH-1)+bottomLeftGetState(i, GRID_WIDTH-1)+bottomGetState(i, GRID_WIDTH-1);
			cellSpace[i][GRID_WIDTH-1].setNextState(count);
		}
		
		cellSpace[0][0].setNextState(bottomGetState(0,0)+rightGetState(0,0)+bottomRightGetState(0,0));

		cellSpace[GRID_HEIGHT-1][0].setNextState(rightGetState(GRID_HEIGHT-1,0)+topRightGetState(GRID_HEIGHT-1,0)+topGetState(GRID_HEIGHT-1,0));

		cellSpace[0][GRID_WIDTH-1].setNextState(leftGetState(0,GRID_WIDTH-1)+bottomLeftGetState(0,GRID_WIDTH-1)+bottomGetState(0,GRID_WIDTH-1));

		cellSpace[GRID_HEIGHT-1][GRID_WIDTH-1].setNextState(leftGetState(GRID_HEIGHT-1,GRID_WIDTH-1)+topGetState(GRID_HEIGHT-1,GRID_WIDTH-1)+topLeftGetState(GRID_HEIGHT-1,GRID_WIDTH-1));

		FinalizeStep();
	}
	
	/**
	 * @param cell The cell we are currently examining
	 * @param vpos current vertical posisiton of the cell
	 * @param hpos current horizontal position of the cell
	 */
	private void centerCellNextStep(GameCell cell, int vpos, int hpos){
		int count = 0;
		count += topRightGetState(vpos, hpos);
		count += rightGetState(vpos, hpos);
		count += bottomRightGetState(vpos, hpos);
		count += bottomGetState(vpos, hpos);
		count += bottomLeftGetState(vpos, hpos);
		count += leftGetState(vpos, hpos);
		count += topLeftGetState(vpos, hpos);
		count += topGetState(vpos, hpos);
		cell.setNextState(count);
	}

	private int topRightGetState(int vpos, int hpos){
		return cellSpace[vpos-1][hpos+1].getCurrentState();
	}
	private int rightGetState(int vpos, int hpos){
		return cellSpace[vpos][hpos+1].getCurrentState();
	}
	private int bottomRightGetState(int vpos, int hpos){
		return cellSpace[vpos+1][hpos+1].getCurrentState();
	}
	private int bottomGetState(int vpos, int hpos){
		return cellSpace[vpos+1][hpos].getCurrentState();
	}
	private int bottomLeftGetState(int vpos, int hpos){
		return cellSpace[vpos+1][hpos-1].getCurrentState();
	}
	private int leftGetState(int vpos, int hpos){
		return cellSpace[vpos][hpos-1].getCurrentState();
	}
	private int topLeftGetState(int vpos, int hpos){
		return cellSpace[vpos-1][hpos-1].getCurrentState();
	}
	private int topGetState(int vpos, int hpos){
		return cellSpace[vpos-1][hpos].getCurrentState();
	}


}
