package main;

public class GameCell {
	
	private int currentState;
	private int nextState;

	public GameCell(int i){
		currentState = i;
	}


	public int getCurrentState(){
		return currentState;
	}

	/** Description of setNextState(int count)
	 * @param count		How many surrounding cells are currently alive
	 */
	public void setNextState(int count){
		if (currentState == 1){
			if (count < 2 || count > 3){
				nextState = 0;
			} else {nextState = 1;}
		}
		if (count == 3 && currentState == 0){
			nextState = 1;
		}
		
	}
	public int swapStates(){
		if (nextState == 0){
			currentState = 0;
		}
		if (nextState == 1){
			currentState = 1;
		}
		nextState = 0;
		return currentState;
	}
}
