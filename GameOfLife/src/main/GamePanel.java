package main;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
/**
 * JPanel that holds the game grid and relevant buttons
 * @author Floris Balm
 *
 */
public class GamePanel extends JPanel {
	private JPanel buttonPanel;
	private JButton speedUpButton, slowDownButton, nextButton, playButton;
	private CellPanel gameView;
	private int gameHeight, gameWidth;
	private double gameSpeed;
	private ArrayList<ArrayList<Integer>> cellStates;
	private GameGrid grid;

	/**
	 * @param height Number of cells vertically
	 * @param width Number of cells horizontally
	 * @param FRAME_HEIGHT Pixelheight of the GameFrame
	 * @param FRAME_WIDTH Pixelwidth of the GameFrame
	 * @param initialState ArrayList of Strings read from comma-separated infile, will
	 * be split and parsed by GameGrid
	 */
	public GamePanel(int height, int width, int FRAME_HEIGHT, int FRAME_WIDTH, ArrayList<String> initialState){
		setLayout(new BorderLayout());
		gameSpeed = 100.0;
		addButtons();
		gameHeight = height;
		gameWidth = width;

		grid = new GameGrid(initialState, gameWidth, gameHeight);
		setVisible(true);
		for(int i = 0; i<gameHeight; ++i){
			for(int j = 0; j<gameWidth; ++j){
				
			}
		}
		cellStates = grid.cellSpaceInt;
		gameView = new CellPanel();
		gameView.setVisible(true);
		add(gameView, BorderLayout.CENTER);
		
	}
	

	private void addButtons(){
		buttonPanel = new JPanel();
		buttonPanel.setBackground(Color.GRAY);
		speedUpButton = new JButton("+");
		speedUpButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				gameSpeed *= 0.95;
				System.out.println(gameSpeed);
			}
		});
		buttonPanel.add(speedUpButton);

		slowDownButton = new JButton("-");
		slowDownButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				gameSpeed *= 1.05;
				System.out.println(gameSpeed);
			}
		});
		slowDownButton.setVisible(true);
		buttonPanel.add(slowDownButton);

		nextButton = new JButton("Next");
		nextButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				grid.nextStep();
				cellStates = grid.cellSpaceInt;
				gameView.revalidate();
				gameView.repaint();
			}
		});
		buttonPanel.add(nextButton);

		playButton = new JButton("Play");
		playButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				for (int i = 0; i<100; ++i){
					grid.nextStep();
					cellStates = grid.cellSpaceInt;
					gameView.revalidate();
					gameView.repaint();
					int sleepTime = (int)(gameSpeed);
					try{
						Thread.sleep(sleepTime);
					} catch (InterruptedException ex){
						ex.printStackTrace();
					}
				}
			}
		});

		buttonPanel.add(playButton); 
		buttonPanel.setVisible(true);

		this.add(buttonPanel, BorderLayout.SOUTH);
		revalidate();
	}
	
	private class CellPanel extends JComponent
	{
		public CellPanel(){
			setBackground(Color.WHITE);
		}
		
		public void paintComponent(Graphics g){
			Graphics2D g2d = (Graphics2D) g;
			for (int i = 0; i<gameHeight; ++i){
				for (int j = 0; j<gameWidth; ++j){
					if (cellStates.get(i).get(j) == 1){
						g2d.setPaint(Color.RED);
						
					} else {
						g2d.setPaint(Color.BLACK);
					}
					g.drawRect(j*15, i*15, 15, 15);
					g.fillRect(j*15, i*15, 15, 15);
				}
			}
			
		}
	}
}
