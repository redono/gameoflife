package main;

import java.awt.EventQueue;

public class GameOfLife {

	public static void main(String[] args) {
	try {
			EventQueue.invokeLater(new Runnable(){
				public void run(){
					GameFrame frame = new GameFrame();
					frame.setVisible(true);
				}
			});
		} catch (Exception ex){
			ex.printStackTrace();
		}
	}

}
