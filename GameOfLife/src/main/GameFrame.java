package main;

import java.awt.BorderLayout;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
/**
 * This is the JFrame holding the GamePanel and the filechooser.
 * @author Floris Balm
 * @version 0.1 Build 1 Sep 15, 2013
 * 
 */
public class GameFrame extends JFrame{

	private int FRAME_HEIGHT;
	private int FRAME_WIDTH;
	private int gameheight;
	private int gamewidth;
	private GamePanel thepanel;
	private ArrayList<String> lines;

	public GameFrame(){
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setTitle("Conway's 'Game of Life', by Me");
		try{
			JFileChooser chooser = new JFileChooser();
			chooser.setCurrentDirectory(new File(".."));
			chooser.showOpenDialog(this);
			
			BufferedReader reader = new BufferedReader(new FileReader(chooser.getSelectedFile()));
			String str = reader.readLine();
			String[] dimensions = str.split(",");
			gameheight = Integer.parseInt(dimensions[0]);
			gamewidth = Integer.parseInt(dimensions[1]);
			
			lines = new ArrayList<String>();
			for(int i = 0; i<gamewidth; ++i){
				String lineRead = reader.readLine();
				lines.add(lineRead);
			}

			FRAME_HEIGHT = 15*gameheight + 150;
			FRAME_WIDTH = 15*gamewidth;
			if (FRAME_WIDTH<500){
				FRAME_WIDTH = 500;
			}
			setSize(FRAME_WIDTH, FRAME_HEIGHT);
			reader.close();
		} catch (Exception ex){
			ex.printStackTrace();
		}
		
		thepanel = new GamePanel(gameheight, gamewidth, FRAME_HEIGHT, FRAME_WIDTH, lines);
		add(thepanel, BorderLayout.CENTER);
		thepanel.setVisible(true);
	
		
	}
}